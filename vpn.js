const redis = require("redis");
const requestHelpers = require("./request-helpers")
const safeJsonStringify = require('safe-json-stringify');

const EXISTING_CLIENTS = 'existing-clients';
const REMOVE_CLIENTS = 'remove-clients';
const NEW_CLIENTS = 'new-clients';
const queryString = require('query-string');

const client = redis.createClient(process.env.REDIS_ENDPOINT);
client.on("error", function (err) {
    console.log("Redis Client Error " + err);
});

// get

module.exports.handleGetExistingVPNClients = function(req, res, metric) {
    client.lrange(EXISTING_CLIENTS, 0, 10000, function(err, data) {
        if (err) {
            requestHelpers.fault(res, metric, err);
            return;
        }

        requestHelpers.succeed(res, metric, safeJsonStringify(data));
    })
};

module.exports.handleGetPendingNewVPNClients = function(req, res, metric) {
    client.lrange(NEW_CLIENTS, 0, 10000, function(err, data) {
        if (err) {
            requestHelpers.fault(res, metric, err);
            return;
        }

        requestHelpers.succeed(res, metric, safeJsonStringify(data));
    })
};


module.exports.handleGetPendingRemoveVPNClients = function(req, res, metric) {
    client.lrange(REMOVE_CLIENTS, 0, 10000, function(err, data) {
        if (err) {
            requestHelpers.fault(res, metric, err);
            return;
        }

        requestHelpers.succeed(res, metric, safeJsonStringify(data));
    })
};

// update / create

const ALLOWED_STRINGS = /^[a-zA-Z0-9\-]*$/;

module.exports.handleAddNewVPNClient = function(req, res, metric) {
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const password = parsed['password'];
    const name = decodeURIComponent(parsed['name']);

    if (!name || !password) {
        requestHelpers.failure(res, metric, 'Name and password must not be empty!');
        return;
    }
    if (password.length < 4 || password.length > 1024) {
        requestHelpers.failure(res, metric, 'Password must be between 4 and 1024 characters!');
        return;
    }
    if (name.search(ALLOWED_STRINGS) === -1) {
        requestHelpers.failure(res, metric, 'Name can only be letters, numbers, and "-"!');
        return;
    }
    if (password.search(ALLOWED_STRINGS) === -1) {
        requestHelpers.failure(res, metric, 'Password can only be letters, numbers, and "-"!');
        return;
    }

    client.lrange(NEW_CLIENTS, 0, 10000, function(err, data) {
        if (err) {
            requestHelpers.fault(res, metric, err);
            return;
        }

        for (let i = 0; i < data.length; i++) {
            const existingClient = JSON.parse(data[i]);

            if (existingClient['name'] === name) {
                requestHelpers.failure(res, metric, 'Request for name: ' + name + ' is already in the queue!');
                return;
            }
        }

        client.lrange(EXISTING_CLIENTS, 0, 10000, function(err, data) {
            if (err) {
                requestHelpers.fault(res, metric, err);
                return;
            }

            if (data.includes(name)) {
                requestHelpers.failure(res, metric, 'client with name: ' + name + ' already exists!');
                return;
            }

            client.rpush(NEW_CLIENTS, safeJsonStringify({'name': name, 'password': decodeURIComponent(parsed['password'])}), function(err) {
                if (err) {
                    requestHelpers.fault(res, metric, err);
                    return;
                }

                requestHelpers.succeed(res, metric, 'Success!');
            })
        });
    });
};

module.exports.handleRemoveVPNClient = function(req, res, metric) {
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));

    const name = decodeURIComponent(parsed['name']);

    if (!name) {
        requestHelpers.failure(res, metric, 'Name must not be empty!');
        return;
    }

    client.lrange(EXISTING_CLIENTS, 0, 10000, function(err, data) {
        if (err) {
            requestHelpers.fault(res, metric, err);
            return;
        }

        if (!data.includes(name)) {
            requestHelpers.failure(res, metric, 'client with name: ' + name + ' does not exist!');
            return;
        }

        client.lpush(REMOVE_CLIENTS, name, function (err) {
            if (err) {
                requestHelpers.fault(res, metric, err);
                return;
            }

            requestHelpers.succeed(res, metric, 'Successfully added ' + name + ' to the remove queue!');
        });
    });
};
