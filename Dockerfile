FROM node:10.15.1

RUN apt-get update && apt-get install -y ruby

COPY ./package.json /app/
COPY ./package-lock.json /app/
RUN cd /app && npm install

COPY start-nodejs.sh.erb /
COPY start.sh /
RUN chmod 777 /start.sh

COPY ./*.js /app/

CMD /start.sh
