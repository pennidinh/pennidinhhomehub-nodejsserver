/usr/bin/erb start-nodejs.sh.erb > start-nodejs.sh

chmod 777 start-nodejs.sh

if [ "$DO_NOT_START" = "true" ]
then
  tail -f /dev/null
else
  ./start-nodejs.sh
fi
