process.on('uncaughtException', function(err) {
  console.log('Caught exception: ' + err + err.stack);
});

const cluster = require('cluster');
const http = require('http');
const safeJsonStringify = require('safe-json-stringify');
const files = require('./files');
const requestHelpers = require("./request-helpers");
const auth = require("./auth");
const plugsAndLights = require("./plugs-and-lights");
const userManagement = require("./user-management");
const info = require("./info");
const vpn = require("./vpn");
const ftp = require("./ftp");

const port = process.env.PORT;
const hostname = '0.0.0.0';

if (cluster.isMaster) {
  console.log(`Master ${process.pid} is running`);

  const sqlite3 = require('sqlite3').verbose();

  const createTablesIndicesAndPopulateCache = function (pennidinhSql, successCallback) {
      console.log('Connected to the SQlite database.');

      const createTablesAndIndices = 'CREATE TABLE IF NOT EXISTS files (file varchar not null, creationTime date not null, type varchar(20) not null, mediaType varchar(20), dirName varchar(20) not null, PRIMARY KEY (type, file)); ' +
          'CREATE INDEX filesCreationTime on files (type, creationTime);  CREATE INDEX filesCreationTimeDirName on files (type, dirName, creationTime);';

      pennidinhSql.run(createTablesAndIndices, [], function(err) {
          if (err) {
              return console.error(err);
          }

          console.log('Table and indices created (or already exists)');
          const startTimeMs = new Date().getTime();

          files.populateFilesCache(process.env.FTP_DIR, function() {
              console.log('Precaching for security files complete after ' + (new Date().getTime() - startTimeMs) + 'ms!');

              files.populateFilesCache('/uploads', function () {

                  console.log('Precaching for upload files complete after ' + (new Date().getTime() - startTimeMs) + 'ms!');
                  if (successCallback) {
                      successCallback();
                  }
              });
          });
      });
  }

  const pennidinhSql = new sqlite3.Database('/tmp/pennidinh', (err) => {
      if (err) {
          return console.error(err.message);
      }

      createTablesIndicesAndPopulateCache(pennidinhSql, function () {
          // Fork workers.
          for (let i = 0; i < parseInt(process.env.NUM_PROCESSES); i++) {
              cluster.fork();
          }

          cluster.on('exit', (worker, code, signal) => {
              console.log(`worker ${worker.process.pid} died`);
              process.exit(1);
          });
      });

      setInterval(function () {
          createTablesIndicesAndPopulateCache(pennidinhSql);
      }, 1000 * 60 * 60 * 24);
    });

  return;
}
console.log(`Worker ${process.pid} started`);

const server = http.createServer((req, res) => {
 const metric = {};
 res.metric = metric;
 const startTimeMs = new Date().getTime();
 const indexOfQ = req.url.indexOf('?');
 let apiName;
 if (indexOfQ == -1) {
   apiName = req.url;
 } else {
   apiName = req.url.substr(0, indexOfQ);
 }
 metric.end = function () {
   console.log(apiName + ' RequestTime (ms): ' + (new Date().getTime() - startTimeMs));
 };
 try {
  console.log(new Date() + " -> " + safeJsonStringify(req.url));
  // info
  if (req.url.indexOf('hostInfo.js') > -1) {
    info.hostInfo(req, res, metric);
    // vpn
  } else if (req.url.indexOf('ftpUsers.json') > -1) {
    ftp.handleGetFtpUsers(req, res, metric);
  } else if (req.url.indexOf('setFtpUser.js') > -1 && req.method !== 'GET') {
    ftp.handleSetFtpUser(req, res, metric);
  } else if (req.url.indexOf('existingVPNClients.js') > -1) {
    vpn.handleGetExistingVPNClients(req, res, metric);
  } else if (req.url.indexOf('pendingNewVPNClients.js') > -1) {
    vpn.handleGetPendingNewVPNClients(req, res, metric);
  } else if (req.url.indexOf('pendingRemoveVPNClients.js') > -1) {
    vpn.handleGetPendingRemoveVPNClients(req, res, metric);
  } else if (req.url.indexOf('addVPNClient.js') > -1 && req.method !== 'GET') {
    vpn.handleAddNewVPNClient(req, res, metric);
  } else if (req.url.indexOf('removeVPNClient.js') > -1 && req.method !== 'GET') {
    vpn.handleRemoveVPNClient(req, res, metric);
  // files
  } else if (req.url.indexOf('saveSecurityFile.js') > -1 && req.method !== 'GET') {
    files.saveSecurityFile(req, res, metric);
  } else if (req.url.indexOf('deleteFile.js') > -1 && req.method !== 'GET') {
    files.handleDeleteFile(req, res, metric);
  } else if (req.url.indexOf('deleteSecurityFile.js') > -1 && req.method !== 'GET') {
    files.handleDeleteSecurityFile(req, res, metric);
  } else if (req.url.indexOf('deleteUploadFile.js') > -1 && req.method !== 'GET') {
    files.handleDeleteUploadFile(req, res, metric);
  } else if (req.url.indexOf('upload.js') > -1 && req.method !== 'GET') {
    files.handleUpload(req, res, metric);
  } else if (req.url.indexOf('popVideoThumbnail.js') > -1 && req.method !== 'GET') {
    files.handlePopVideoThumbnail(req, res, metric);
  } else if (req.url.indexOf('pushVideoThumbnail.js') > -1 && req.method !== 'GET') {
    files.handlePushVideoThumbnail(req, res, metric);
  } else if (req.url.indexOf('fileDeletedEvent.js') > -1 && req.method !== 'GET') {
    files.handleFileDeletedEvent(req, res, metric);
  } else if (req.url.indexOf('pushVideoThumbnailsBatch.js') > -1 && req.method !== 'GET') {
      files.handlePushVideoThumbnailsBatch(req, res, metric);
  } else if (req.url.indexOf('pendingVideoThumbnails.json') > -1) {
      files.handleGetPendingVideoThumbnails(req, res, metric);
  } else if (req.url.indexOf('pendingVideoThumbnailsCount.json') > -1) {
      files.handleGetPendingVideoThumbnailsCount(req, res, metric);
  } else if (req.url.indexOf('securityFiles.json') > -1) {
    files.handleGetSecurityFiles(req, res, metric);
  } else if (req.url.indexOf('securityFilesV2.json') > -1) {
    files.handleGetSecurityFilesV2(req, res, metric);
  } else if (req.url.indexOf('securityFileCameraNames.json') > -1) {
    files.handleGetSecurityCameraNames(req, res, metric);
  } else if (req.url.indexOf('uploadFiles.json') > -1) {
      files.handleGetUploadFiles(req, res, metric);
  } else if (req.url.indexOf('fileSystemStats.json') > -1) {
      files.handleGetFileSystemStats(req, res, metric);
  } else if (req.url.indexOf('makeUploadFilePublic.js') > -1 && req.method !== 'GET') {
      files.makeUploadFilePublic(req, res, metric);
    // user management
  } else if (req.url.indexOf('getAdminUsers.json') > -1) {
    userManagement.getAdminUsers(req, res, metric);
  } else if (req.url.indexOf('removeAdminUsers.js') > -1 && req.method !== 'GET') {
    userManagement.removeAdminUsers(req, res, metric);
  } else if (req.url.indexOf('addAdminUsers.js') > -1 && req.method !== 'GET') {
    userManagement.addAdminUsers(req, res, metric);
    // auth
  } else if (req.url.indexOf('setLoginCookieAndRedirect.js') > -1) {
    auth.handleSetLoginCookieAndRedirect(req, res, metric);
  } else if (req.url.indexOf('authentication-ping.json') > -1) {
    auth.handleAuthenticationPing(req, res, metric);
  } else if (req.url.indexOf('authorization-ping.json') > -1) {
    auth.handleAuthorizationPing(req, res, metric);
  } else if (req.url.indexOf('getLights.json') > -1) {
    plugsAndLights.handleGetLights(req, res, metric);
  } else if (req.url.indexOf('logout.js') > -1) {
    auth.handleLogout(req, res, metric);
  } else if (req.url.indexOf('loginV2.js') > -1) {
      auth.handleLoginV2(req, res, metric);
  } else if (req.url.indexOf('loginSuccess.js') > -1) {
      auth.handleLoginSuccess(req, res, metric);
  } else if (req.url.indexOf('profile.js') > -1) {
      auth.handleProfile(req, res, metric);
  } else if (req.url.indexOf('getAccessToken.js') > -1) {
      auth.handleGetAccessToken(req, res, metric);
  // request headers for debugging
  } else if (req.url.indexOf('requestHeaders.json') > -1) {
    res.setHeader('Content-Type', 'application/json');
    requestHelpers.succeed(res, metric, safeJsonStringify(req.headers));
  // user context
  } else if (req.url.indexOf('userInfo.json') > -1) {
    res.setHeader('Content-Type', 'application/json');
    requestHelpers.succeed(res, metric, safeJsonStringify({name: req.headers.oidc_claim_name, email: req.headers.oidc_claim_email}));
  // plugs and lights (and leaving and arriving at the house)
  } else if (req.url.indexOf('setLight.json') > -1 && req.method !== 'GET') {
    plugsAndLights.handleSetLight(req, res, metric);
  } else if (req.url.indexOf('leaveTheHouse.json') > -1 && req.method !== 'GET') {
    plugsAndLights.handleLeaveTheHouse(req, res, metric);
  } else if (req.url.indexOf('arriveAtTheHouse.json') > -1 && req.method !== 'GET') {
     plugsAndLights.handleArriveAtTheHouse(req, res, metric);
  } else if (req.url.indexOf('getPlugDevices.json') > -1) {
    plugsAndLights.handleGetPlugDevices(req, res, metric);
  } else if (req.url.indexOf('setPlugDevice.json') > -1 && req.method !== 'GET') {
    plugsAndLights.handleSetPlugDevice(req, res, metric);
  // not found
  } else {
    res.setHeader('Content-Type', 'text/plain');
    console.log('Couldn\'t find handler');
    requestHelpers.notFound(res, metric);
  }
 } catch (error) {
  console.log("error processing request: " + error + error.stack);
  requestHelpers.fault(res, metric, error + error.stack);
 }
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
