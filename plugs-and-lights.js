const request = require('request');
const queryString = require('query-string');
const { Client } = require('tplink-smarthome-api');
const safeJsonStringify = require('safe-json-stringify');
const requestHelpers = require("./request-helpers")

function getAllLights(callback) {
    request('http://phillipslightshub.pennidinh.com/api/FNZpuj4mc1OUs4Tsq0zwaQ22oGlbLSwTE-vP-ALa/lights', function(error, response, body) {
        callback(error, response, body);
    });
}

function setLight(lightIndex, onString, briString, callback) {
    const on = onString ? onString == 'true' : true;
    const bri = briString && on ? parseInt(briString) : null;

    const objectForPhillipsHue = {on: on};
    if (bri) {
        objectForPhillipsHue.bri = bri;
    }

    request({url: 'http://phillipslightshub.pennidinh.com/api/FNZpuj4mc1OUs4Tsq0zwaQ22oGlbLSwTE-vP-ALa/lights/' + lightIndex + '/state', method: 'PUT', body: safeJsonStringify(objectForPhillipsHue)}, function(error, response, body) {
        callback(error, response, body);
    });
}

module.exports.handleSetPlugDevice = function (req, res, metric) {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');

    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));

    console.log('hostIP: ' + parsed.hostIP);
    console.log('hostPort: ' + parsed.hostPort);
    console.log('value: ' + parsed.value);
    const newPowerState = parsed.value;

    const client = new Client();
    client.getDevice({host: parsed.hostIP, port: parsed.hostPort}).then((device)=>{
        if (newPowerState != 'true' && newPowerState != 'false') {
            device.getPowerState().then(powerState => device.setPowerState(!powerState));
        } else {
            device.setPowerState(newPowerState == 'true');
        }
        requestHelpers.succeed(res, metric);
    });
}

module.exports.handleSetLight = function (req, res, metric) {
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));

    setLight(parsed.lightIndex, parsed.on, parsed.bri, function(error, response, body) {
        res.statusCode = response ? parseInt(response.statusCode) : 500;
        res.setHeader('Content-Type', 'application/json');

        res.end(body);
        metric.end();
    });
}

module.exports.handleGetPlugDevices = function (req, res, metric) {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');

    const client = new Client();
    let devices = []
    client.startDiscovery({devices: [{host: '10.0.0.162'}, {host: '10.0.0.98'}, {host: '10.0.0.84'}, {host: '10.0.0.181'}]}).on('device-new', (device) => {
        console.log("device: => " + safeJsonStringify(device));
        devices.push({outletIP: device.host, outletPort: device.port, friendlyName: device._sysInfo.alias, productName: device._sysInfo.dev_name, powerState: device.lastState.inUse, relayState: device.lastState.relayState, model: device._sysInfo.model});
    });

    setTimeout(function(){
        requestHelpers.succeed(res, metric, safeJsonStringify(devices));
    }, 100);
}

module.exports.handleArriveAtTheHouse = function (req, res, metric) {
    getAllLights(function(error, response, body) {
        let i = 0;

        if (error) {
            res.setHeader('Content-Type', 'text/html');
            requestHelpers.fault(res, metric, 'Error returned from lights: ' + error);
            return;
        }

        Object.keys(JSON.parse(body)).forEach(function(key) {
            setTimeout(function () {
                    setLight(key, "true", 200, function() {});
                }
                , 100 * i);

            i = i + 1;
        });

        res.setHeader('Content-Type', 'application/json');

        requestHelpers.succeed(res, metric);
    });
}

module.exports.handleLeaveTheHouse = function (req, res, metric) {
    getAllLights(function(error, response, body) {
        let i = 0;

        if (error) {
            res.setHeader('Content-Type', 'text/html');
            requestHelpers.fault(res, metric, 'Error returned from lights: ' + error);
            return;
        }

        Object.keys(JSON.parse(body)).forEach(function(key) {
            setTimeout(function () {
                    setLight(key, "false", null, function() {});
                }
                , 100 * i);

            i = i + 1;
        });

        res.setHeader('Content-Type', 'application/json');

        requestHelpers.succeed(res, metric);
    });
}

module.exports.handleGetLights = function (req, res, metric) {
    getAllLights(function(error, response, body) {
        res.statusCode = response ? parseInt(response.statusCode) : 500;
        res.setHeader('Content-Type', 'application/json');

        res.end(body);
        metric.end();
    });
}