const recursiveReaddir = require("recursive-readdir");
const redis = require("redis");
const formidable = require('formidable');
const queryString = require('query-string');
const safeJsonStringify = require('safe-json-stringify');
const requestHelpers = require("./request-helpers")
const fs = require('fs');
const path = require('path');
const disk = require('diskusage');

const client = process.env.REDIS_ENDPOINT ? redis.createClient(process.env.REDIS_ENDPOINT) : redis.createClient(); // since reads can block and these clients cannot handle multiple concurrent requests, have a dedicated client for reads
client.on("error", function (err) {
    console.log('Redis error: ' + err);
});
const clientForBlocking = process.env.REDIS_ENDPOINT ? redis.createClient(process.env.REDIS_ENDPOINT) : redis.createClient();
clientForBlocking.on("error", function (err) {
    console.log("Redis ClientForBlocking Error " + err);
});

const sqlite3 = require('sqlite3').verbose();

const pennidinhSql = new sqlite3.Database('/tmp/pennidinh', (err) => {
    if (err) {
        return console.error(err.message);
    }
    console.log('Connected to the SQlite database.');
});

function getFileType(filePath) {
    return (filePath.indexOf(process.env.FTP_DIR) === 0) ? 'SECURITY_UPLOADS' : 'GENERAL_UPLOADS';
}

function getCameraName(filePath) {
    return filePath.split('/')[4];
}

function chunk(array, size) {
    const chunked_arr = [];
    for (let i = 0; i < array.length; i++) {
        const last = chunked_arr[chunked_arr.length - 1];
        if (!last || last.length === size) {
            chunked_arr.push([array[i]]);
        } else {
            last.push(array[i]);
        }
    }
    return chunked_arr;
}

const getMediaType = function(filename) {
    if (filename.endsWith('jpg') || filename.endsWith('JPG') || filename.endsWith('jpeg') || filename.endsWith('JPEG') || filename.endsWith('png')  || filename.endsWith('PNG')) {
        return null;
    } else if (filename.endsWith('mp4') || filename.endsWith('MP4') || filename.endsWith('MOV') || filename.endsWith('mov') || filename.endsWith('264'))  {
        return "video";
    } else {
        return null;
    }
}

const populateFilesCache = function(dir, successCallback) {
    const filesToReturn = [];

    recursiveReaddir(dir, (err, files) => {
        if (err) {
            console.log(err);
        } else {
            files.forEach(file => {
                if (file.indexOf('thumbnail') <= 0 && !file.endsWith('.idx')) {
                  filesToReturn.push(file);
                } else {
                }
            });
        }

        const filesWithMetadata = [];

        const handleFilesWithMetadata = function () {
            const chunkedFilesWithMetadata = chunk(filesWithMetadata, 100);

            const insertBatch = function (iterationNum) {
                if (iterationNum === chunkedFilesWithMetadata.length) {
                    successCallback();

                    return;
                }

                const filesWithMetadataChunk = chunkedFilesWithMetadata[iterationNum];
                const sqlParams = filesWithMetadataChunk.map(function(fileWithMetadata) {
                    const type = getFileType(fileWithMetadata.filePath);
                    const dirName = type === 'SECURITY_UPLOADS' ? getCameraName(fileWithMetadata.filePath) : 'PENNIDINH_FILE_UPLOAD';
                    const mediaType = getMediaType(fileWithMetadata.filePath);

                    return [type, fileWithMetadata.filePath, dirName, mediaType, fileWithMetadata.creationTime]
                });
                const sqlParam = [].concat.apply([], sqlParams);

                const stringChunks = [];
                for (let i = 0; i < filesWithMetadataChunk.length; i++) {
                    stringChunks.push(' ( ? , ?, ? , ?, ? ) ');
                }

                const insertSql = 'insert or ignore into files (type, file, dirName, mediaType, creationTime) values ' + stringChunks.join(', ');

                pennidinhSql.run(insertSql, sqlParam, function(err) {
                    if (err) {
                        console.log(err);
                        process.exit(1);
                    }

                    insertBatch(iterationNum + 1);
                })
            }
            insertBatch(0);
        };

        const populateMetadata = function(index) {
            if (index === filesToReturn.length) {
                handleFilesWithMetadata();
                return;
            }

            fs.stat(filesToReturn[index], function(err, response) {
                if (err) {
                    console.log('assuming file was removed: ' + filesToReturn[index]);
                } else {
                    filesWithMetadata.push({creationTime: response.birthtime, filePath: filesToReturn[index]});
                }

                populateMetadata(index + 1);
            });
        };
        populateMetadata(0);
    });
};
module.exports.populateFilesCache = populateFilesCache;

const getFilesSQL = function(type, dirNames, mediaTypes, limit, offset, orderBySQL) {

    let dirNamesSQL;
    if (dirNames) {
        dirNamesSQL = ' AND dirName IN ( '  + dirNames.map(function(dirName){ return '\'' + dirName + '\'' }).join(',') +  ' ) ';
    } else {
        dirNamesSQL = '';
    }

    let mediaTypesSQL;
    if (mediaTypes != null) {
        mediaTypesSQL = ' AND mediaType IN ( '  + mediaTypes.map(function(mediaType){ return '\'' + mediaType + '\'' }).join(',') +  ' ) ';
    } else {
        mediaTypesSQL = '';
    }

    let offsetSQL;
    if (offset) {
        offsetSQL = ' OFFSET ' + offset + ' ';
    } else {
        offsetSQL = '';
    }

    let limitSQL;
    if (limit) {
        limitSQL = ' LIMIT ' + limit + ' ';
    } else {
        limitSQL = '';
    }

    return [' from files where type = ? ' + dirNamesSQL + mediaTypesSQL + orderBySQL + limitSQL + offsetSQL, [type]];
}

const getFiles = function(res, metric, type, dirNames, mediaTypes, limit, offset, successCallback) {
    const sql = getFilesSQL(type, dirNames, mediaTypes, limit, offset, ' order by creationTime desc ');
    pennidinhSql.all('select file, creationTime, dirName, mediaType ' + sql[0], sql[1], function (err, rows) {
        if (err) {
            console.log(err);
            requestHelpers.fault(res, metric, err);
            return;
        }

        const response = rows.map(function(row) {
            return {filePath: row.file, creationTimeMs: row.creationTime, name: row.dirName, mediaType: row.mediaType};
        });

        successCallback(safeJsonStringify(response));
    });
};

// single pushes are pushed to front of queue
module.exports.handleFileDeletedEvent = function (req, res, metric) {
    res.setHeader('Content-Type', 'application/json');

    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const thumbnailFilePath = decodeURIComponent(parsed.thumbnailFilePath);

    if (thumbnailFilePath == null || thumbnailFilePath == '') {
        res.statusCode = 422;
        res.end('thumbnailFilePath param must not be empty!');
        metric.end();
        return;
    }
    res.end('asych acking this request. check service logs for result');

    if (thumbnailFilePath.indexOf('thumbnail') > -1) {
        requestHelpers.succeed(res, metric, 'No-op push of existing thumbnail');
        return;
    }

    console.log('thumbnailFilePath: ' + thumbnailFilePath);
    const type = getFileType(thumbnailFilePath);
    console.log('type: ' + type);

    const deleteFileSql = 'delete from files where type = ? and file = ? ;';

    pennidinhSql.run(deleteFileSql, [type, thumbnailFilePath], function(err) {
        if (err) {
            console.log(err);
            return;
        }

        console.log('File deleted (or no-op) successfully!');
    })
}

// single pushes are pushed to front of queue
module.exports.handlePushVideoThumbnail = function (req, res, metric) {
    res.setHeader('Content-Type', 'application/json');

    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const thumbnailFilePath = decodeURIComponent(parsed.thumbnailFilePath);

    if (thumbnailFilePath == null || thumbnailFilePath == '') {
        res.statusCode = 422;
        res.end('thumbnailFilePath param must not be empty!');
        metric.end();
        return;
    }
    res.end('asych acking this request. check service logs for result');

    if (thumbnailFilePath.indexOf('thumbnail') > -1) {
        requestHelpers.succeed(res, metric, 'No-op push of existing thumbnail');
        return;
    }
    if (thumbnailFilePath.indexOf('/uploads/.tmp') === 0) {
        requestHelpers.succeed(res, metric, 'Ignoring temporary upload file');
        return;
    }

    fs.stat(thumbnailFilePath, function (err, stats) {
        if (err) {
            console.log(err);
            return;
        }

        const dir = (thumbnailFilePath.indexOf(process.env.FTP_DIR) === 0) ? process.env.FTP_DIR : '/uploads';
        console.log('dir: ' + dir);
        const type = getFileType(thumbnailFilePath);
        console.log('type: ' + type);
        const dirName = type === 'SECURITY_UPLOADS' ? getCameraName(thumbnailFilePath) : 'PENNIDINH_FILE_UPLOAD';
        console.log('dirName: ' + dirName);
        const mediaType = getMediaType(thumbnailFilePath);

        const insertFileSql = 'insert or ignore into files (type, file, dirName, mediaType, creationTime) values ( ? , ? , ? , ? , ? ) ;';
        pennidinhSql.run(insertFileSql, [type, thumbnailFilePath, dirName, mediaType, stats.birthtime], function(err) {
            if (err) {
                console.log(err);
                return;
            }

            client.lpush('video-thumbnails', thumbnailFilePath, function(err) {
                if (err != null) {
                    requestHelpers.fault(res, metric, 'Internal error pushing data to redis client: ' + err);
                } else {
                    requestHelpers.succeed(res, metric);
                }
            });
        });
    });
}

// batch pushes are pushed to back of queue
module.exports.handlePushVideoThumbnailsBatch = function (req, res, metric) {
    res.setHeader('Content-Type', 'application/json');

    let body = '';
    req.on('error', err => {
        console.log(err)
    });
    req.on('data', chunk => {
        console.log('chunk with end');
        body = body + chunk.toString();
    });
    req.on('end', (chunk) => {
        if (chunk) {
            console.log('chunk with end');
            body = body + chunk.toString();
        }

        console.log('Parsing uploading thumbnail file paths...');
        const thumbnailFilePaths = JSON.parse(body);
        console.log('Parsed thumbnail file paths successfully');

        // hack assuming first files dir is equal to all files dir
        const dir = (thumbnailFilePaths.length > 0 && thumbnailFilePaths[0].indexOf(process.env.FTP_DIR) === 0) ? process.env.FTP_DIR : '/uploads';

        client.rpush('video-thumbnails', thumbnailFilePaths, function (err) {
            if (err) {
                requestHelpers.fault(res, metric, err);
                return;
            }

            requestHelpers.succeed(res, metric);
        });
    });
}

module.exports.handlePopVideoThumbnail = function (req, res, metric) {
    res.setHeader('Content-Type', 'application/json');

    let requestActive = true;
    req.on('close', function() {
        requestActive = false;
    });

    clientForBlocking.blpop('video-thumbnails', 100, function(err, data) {
        if (err != null) {
            requestHelpers.fault(res, metric, err);
            return;
        }
        if (data == null) {
            res.statusCode = 204;
            res.end();
            metric.end();
            return;
        }

        if (!requestActive) {
            console.log('Work item returned by blockin redis call but node server request has closed unexpectedly by client before the item was returned');
            client.lpush('video-thumbnails', data[1], function(err) {
                if (err) {
                    console.log(err);
                    return;
                }

                console.log('Work item successfully put back in the queue');
            });
        } else {
            console.log('Blocking request for work item returned data: ' + data[1]);

            requestHelpers.succeed(res, metric, encodeURIComponent(data[1]));
        }
    });
}

module.exports.handleUpload = function (req, res, metric) {
    const form = new formidable.IncomingForm();
    form.keepExtensions = true;
    form.type = 'multipart';
    form.maxFields = 10;
    form.maxFileSize = 5 * 1024 * 1024 * 1024;
    form.multiples = true;
    form.uploadDir = '/uploads/.tmp';

    form.on('file', function(name, file) {
        fs.rename(file.path, "/uploads/" + file.name, function(err) {
            if (err) {
                console.log('Error renaimin file: ' + err);
            }
        });
    });

    form.parse(req, function (err, fields, files) {
        if (err) {
            requestHelpers.fault(res, metric, 'error uploading files: ' + err);
            return;
        }

        let resStr = '';
        for (let i = 0 ; i < files.length ; i++) {
            const file = files[i];

            resStr = resStr + file.name;
        }
        requestHelpers.succeed(res, metric, resStr);
    });
}

module.exports.handleDeleteFile = function (req, res, metric) {
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const file = decodeURIComponent(parsed.file);
    fs.unlink(file, (err) => {
        if (err != null) {
            requestHelpers.notFound(res, metric, 'Could not delete requested file with error: ' + err);
            return;
        }

        requestHelpers.succeed(res, metric, 'File deleted successfully!');
    });
}

module.exports.handleDeleteUploadFile = function (req, res, metric) {
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const file = decodeURIComponent(parsed.file);

    if (file.indexOf('/uploads/') !== 0) {
        requestHelpers.failure(res, metric, 'expected file ' + file + ' to begin with /uploads/');
        return;
    }

    fs.unlink(file, (err) => {
        if (err != null) {
            requestHelpers.notFound(res, metric, 'Could not delete requested file with error: ' + err);
            return;
        }

        requestHelpers.succeed(res, metric, 'File deleted successfully!');
    });
}

module.exports.handleDeleteSecurityFile = function (req, res, metric) {
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const file = decodeURIComponent(parsed.file);

    fs.unlink(process.env.FTP_DIR + '/' + file, (err) => {
        if (err != null) {
            requestHelpers.notFound(res, metric, 'Could not delete requested file with error: ' + err);
            return;
        }

        requestHelpers.succeed(res, metric, 'File deleted successfully!');
    });
}

module.exports.handleGetSecurityCameraNames = function (req, res, metric) {
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));

    pennidinhSql.all('select DISTINCT dirName as cameraName from files where type = \'SECURITY_UPLOADS\'', function (err, rows) {
        if (err) {
            console.log(err);
            requestHelpers.fault(res, metric, err);
            return;
        }

        res.setHeader('Content-Type', 'application/json');
        requestHelpers.succeed(res, metric, {cameraNames: rows.map(x => x.cameraName).filter(x => x != 'mkdir.db')});
    });
    
}

module.exports.handleGetSecurityFilesV2 = function (req, res, metric) {
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const dirNames = parsed.dirNames != null ? parsed.dirNames.split(',') : null;
    const mediaTypes = parsed.mediaTypes != null ? parsed.mediaTypes.split(',') : null;
    const limit = parsed.limit;
    const offset = parsed.offset;

    getFiles(res, metric, 'SECURITY_UPLOADS', dirNames, mediaTypes, limit, offset, function(getFilesResponse) {
        const sql = getFilesSQL('SECURITY_UPLOADS', dirNames, mediaTypes, 0, 0, '');
        pennidinhSql.get('select count (*) as total ' + sql[0], sql[1], function (err, row) {
            if (err) {
                console.log(err);
                requestHelpers.fault(res, metric, err);
                return;
            }

            const totalCount = row.total;

            res.setHeader('Content-Type', 'application/json');
            requestHelpers.succeed(res, metric, {files: JSON.parse(getFilesResponse), count:totalCount});
        });
    });
}

module.exports.handleGetSecurityFiles = function (req, res, metric) {
    getFiles(res, metric, 'SECURITY_UPLOADS', null, null, 0, 0, function(response) {
        res.setHeader('Content-Type', 'application/json');
        requestHelpers.succeed(res, metric, response);
    });
}

module.exports.handleGetUploadFiles = function (req, res, metric) {
    getFiles(res, metric, 'GENERAL_UPLOADS', null, null, 0, 0, function(response) {
        res.setHeader('Content-Type', 'application/json');
        requestHelpers.succeed(res, metric, response);
    });
}

module.exports.handleGetPendingVideoThumbnails = function(req, res, metric) {
    client.lrange('video-thumbnails', 0, 100000, function(err ,data) {
        if (err) {
            console.log(err);
            requestHelpers.fault(res, metric, err);
            return;
        }

        requestHelpers.succeed(res, metric, safeJsonStringify(data));
    });
}

module.exports.handleGetPendingVideoThumbnailsCount = function(req, res, metric) {
    client.llen('video-thumbnails', function(err, data) {
        if (err) {
            console.log(err);
            requestHelpers.fault(res, metric, err);
            return;
        }

        requestHelpers.succeed(res, metric, safeJsonStringify({count: data}));
    });
}

module.exports.handleGetFileSystemStats = function(req, res, metric) {
    disk.check('/', function(err, info) {
        if (err) {
            requestHelpers.fault(res, metric, err);
            return;
        }

        requestHelpers.succeed(res, metric, safeJsonStringify(info));
    });
}

module.exports.saveSecurityFile = function(req, res, metric) {
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const file = decodeURIComponent(parsed.file);
    const isPublic = parsed.isPublic === 'true';

    const destFileName = '/uploads' + (isPublic ? '/public' : '') + file;
    fs.mkdir(path.dirname(destFileName), { recursive: true }, (err) => {
      if (err) {
        console.log(err);
        return;
      }

      fs.copyFile(process.env.FTP_DIR + file, destFileName, (err) => {
          if (err) {
            console.log(err);
            return;
          }

          requestHelpers.succeed(res, metric, '');
      });
    });
}

module.exports.makeUploadFilePublic = function(req, res, metric) {
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const file = decodeURIComponent(parsed.file);

    if (file.indexOf('/uploads/') !== 0) {
        requestHelpers.failure(res, metric, 'expected file ' + file + ' to begin with /uploads/');
        return;
    }

    const destFileName = '/uploads/public/' + file.substring('/uploads/'.length);
    fs.mkdir(path.dirname(destFileName), { recursive: true }, (err) => {
        if (err) {
            console.log(err);
            return;
        }

        fs.rename(file, destFileName, (err) => {
            if (err) {
                console.log(err);
                return;
            }

            requestHelpers.succeed(res, metric, '');
        });
    });
}
