const base64 = require('base-64');
const request = require('request');
const queryString = require('query-string');
const safeJsonStringify = require('safe-json-stringify');
const requestHelpers = require("./request-helpers");
const encrypt = require('./encrypt');
const fs = require('fs');
const uuidv1 = require('uuid/v1');

// START DATASTORE
const redis = require("redis");
const client = process.env.REDIS_ENDPOINT ? redis.createClient(process.env.REDIS_ENDPOINT) : redis.createClient(); // since reads can block and these clients cannot handle multiple concurrent requests, have a dedicated client for reads
client.on("error", function (err) {
    console.log("Redis Client Error " + err);
});
// END DATASTORE

module.exports.handleSetLoginCookieAndRedirect = function(req, res, metric) {
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const accessToken = parsed.accessToken;
    const redirectTo = decodeURIComponent(parsed.redirect_to);
    const cookies = [];

    const t = new Date();
    t.setSeconds(t.getSeconds() +  60 * 55);
    cookies.push('expiringToken=' + accessToken + '; Path=/; Secure;');

    if (parsed['sharedProxyPort']) {
        cookies.push('pennidinhProxyAddress=' + parsed['sharedProxyPort'] + '; Path=/; Secure;');
    }

    console.log('Setting expiring token cookie to: ' + accessToken);
    console.log('Redirecting to: ' + redirectTo);

    res.statusCode = 303;
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Location', redirectTo);
    res.setHeader('Set-Cookie', cookies);
    res.end();
    metric.end();
}

module.exports.handleLogout = function (req, res, metric) {
    res.statusCode = 303;
    res.setHeader('Location', '/');
    const cookies = ['expiringToken=; Expires=Sun, 15 Jul 2010 00:00:01 GMT; Path=/; Secure;'];

    res.setHeader('Set-Cookie', cookies);

    res.end();
    metric.end();
};

const getFreshEncryptedPenniDinhTokenHelper = function(res, metric, decryptedPennidinhToken, appSecretKey, successCallback, attempt=0) {
    if (attempt >= 3) {
        console.log('Too many attempts failed to get fresh pennidinh token');
        client.del(appSecretKey, function(err) {
            if (err) {
                requestHelpers.fault(res, metric, 'Too many attempts failed to get fresh pennidinh token. Delete appSecretKey.');
                return;
            }

            requestHelpers.fault(res, metric, 'Too many attempts failed to get fresh pennidinh token. Delete appSecretKey.');
        });                           
	return;
    }
    request({uri: 'https://auth.api.pennidinh.com?provider=' + encodeURIComponent(decryptedPennidinhToken['provider']) + '&pennidinhToken=' + encodeURIComponent(decryptedPennidinhToken['encryptedPennidinhToken']) + '&clientId=' + process.env.PENNIDINH_CLIENT_ID}, function(err, httpResponse, encryptedResponse) {
        if (err) {
	    console.log('Error thrown while fetching token. Trying again. ' + err);
	    setTimeout(function() {
                getFreshEncryptedPenniDinhTokenHelper(res, metric, decryptedPennidinhToken, appSecretKey, successCallback, attempt + 1);
            }.bind(this), Math.pow(2 , attempt + 1) * 500);
            return;
        }

        if (httpResponse.statusCode !== 200) {
            console.log('Non-200 response. Trying again.');
            console.log(safeJsonStringify(httpResponse));
	    setTimeout(function() {
                getFreshEncryptedPenniDinhTokenHelper(res, metric, decryptedPennidinhToken, appSecretKey, successCallback, attempt + 1);
            }.bind(this), Math.pow(2 , attempt + 1) * 500);
            return;
        }

        client.ttl(appSecretKey, function(err, ttl) {
            if (err) {
		console.log('Error reading app secret key ttl from redis. Trying again ' + err);
       	        setTimeout(function() {
                    getFreshEncryptedPenniDinhTokenHelper(res, metric, decryptedPennidinhToken, appSecretKey, successCallback, attempt + 1);
                }.bind(this), Math.pow(2 , attempt + 1) * 500);
                return;
            }

            const ttlWriteCallback = function (err) {
                if (err) {
      		    console.log('Error writing app secret key ttl from redis. Trying again ' + err);
           	        setTimeout(function() {
                        getFreshEncryptedPenniDinhTokenHelper(res, metric, decryptedPennidinhToken, appSecretKey, successCallback, attempt + 1);
                    }.bind(this), Math.pow(2 , attempt + 1) * 500);
                    return;
                }

                successCallback(encryptedResponse);
            }.bind(this);

            console.log('ttl: ' + ttl);

            if (ttl === -1) {
                client.set(appSecretKey, encryptedResponse, ttlWriteCallback);
            } else {
                client.setex(appSecretKey, ttl, encryptedResponse, ttlWriteCallback);
            }
        }.bind(this));
    }.bind(this));
}

const getFreshEncryptedPenniDinhToken = function(appSecretKey, res, metric, successCallback) {
    client.get(appSecretKey, function(err, encryptedPenniDinhToken) {
        if (err) {
            requestHelpers.fault(res, metric, err);
            return;
        }
        if (!encryptedPenniDinhToken) {
            requestHelpers.failure(res, metric, 'Couldnt find profile for app secret key: ' + appSecretKey);
            return;
        }
        console.log('encrypted pennidinh token: ' + encryptedPenniDinhToken);

        fs.readFile('/keys/private.pem', 'utf-8', function(err, hubPrivateKey) {
            if (err) {
                requestHelpers.fault(res, metric, err);
                return;
            }

            encrypt.decryptWithPrivateKey(hubPrivateKey, encryptedPenniDinhToken, res, metric, function(decryptedPennidinhToken) {
                console.log('Decrypted pennidinh token: '+ safeJsonStringify(decryptedPennidinhToken));

                if (decryptedPennidinhToken['time'] > (new Date().getTime() / 1000) + 60 * 5) {
                    successCallback(encryptedPenniDinhToken);
                } else {
	            console.log('Existing pennidinh token is expired. Fetching new one from auth.api.pennidinh.com');
                    const provider = decryptedPennidinhToken['provider'];
                    if (!provider) {
                        client.del(appSecretKey, function(err) {
                            if (err) {
                                requestHelpers.fault(res, metric, err);
                                return;
                            }

                            requestHelpers.fault(res, metric, 'Existing decryptedPennidinhToken did not have a "provider". Deleted token. Please try again.');
                        });
                        return;
                    }

		    getFreshEncryptedPenniDinhTokenHelper(res, metric, decryptedPennidinhToken, appSecretKey, successCallback);
                }
            }.bind(this))
        }.bind(this));
    }.bind(this));
};

module.exports.handleGetAccessToken = function(req, res, metric) {
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const appSecretKey = parsed.appSecretKey;
 
    if (!appSecretKey) {
        requestHelpers.failure(res, metric, 'appSecretKey param must be present!')
        return;
    }

    client.exists(appSecretKey, function(err, existsResult) {
        if (err) {
            requestHelpers.fault(res, metric, err);
            return;
        }
        if (existsResult) {
            getFreshEncryptedPenniDinhToken(appSecretKey, res, metric, function(encryptedPenniDinhToken) {
                fs.readFile('/keys/private.pem', 'utf-8', function(err, hubPrivateKey) {
                    if (err) {
                        requestHelpers.fault(res, metric, err);
                        return;
                    }

                    console.log('decrypting PenniDinh token');
                    encrypt.decryptWithPrivateKey(hubPrivateKey, encryptedPenniDinhToken, res, metric, function (decryptedResponse) {
                        const expiringToken = uuidv1();

                        const expirationEpochTime = Math.min((new Date().getTime() / 1000) + (60 * 55), decryptedResponse['time'] - (new Date().getSeconds() / 1000));
                        const expirationSeconds = Math.floor(expirationEpochTime - (new Date().getTime() / 1000));
                        console.log('expirationEpochTime: ' + expirationEpochTime);
                        console.log('expirationSeconds: ' + expirationSeconds);

                        client.setex(expiringToken, expirationSeconds, appSecretKey, function(err) {
                            if (err) {
                                requestHelpers.fault(res, metric, err);
                                return;
                            }

                            requestHelpers.succeed(res, metric, safeJsonStringify({accessTokenExpirationEpochTime: expirationEpochTime, accessToken: expiringToken}));
                        });
                    });
                });
            });        
        } else {
          requestHelpers.failure(res, metric, 'appSecretKey ' + appSecretKey + ' could not be found!')
          return;
        }
    });

}

module.exports.handleLoginV2 = function(req, res, metric) {
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const isAppLogin = parsed.isAppLogin;
    const redirectTo = decodeURIComponent(parsed.redirectTo || '/');
    let hostname = decodeURIComponent(parsed.hostname);

    if (!hostname) {
        requestHelpers.failure(res, metric, 'hostname param must be present!')
        return;
    }

    const provider = parsed['provider'];
    if (!provider) {
        requestHelpers.failure(res, metric, 'Provider param must be present!');
        return;
    }

    const redirectToAfterSuccess = 'https://' + hostname + '/secure-node-js/loginSuccess.js?redirectTo=' + encodeURIComponent(redirectTo);

    const redirectTo2 = 'https://auth.api.pennidinh.com?provider=' + provider + '&clientId=' + process.env.PENNIDINH_CLIENT_ID + '&browserSessionOnly=' + !isAppLogin + '&redirectTo=' + encodeURIComponent(redirectToAfterSuccess);
    res.statusCode = 303;
    res.setHeader('Location', redirectTo2);
    res.end();
    metric.end();
}

module.exports.handleLoginSuccess = function(req, res, metric) {
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const redirectTo = parsed.redirectTo;
    const appSecretKey = uuidv1();
    const pennidinhKey = new Buffer(decodeURIComponent(parsed.pennidinhKey), 'base64');
    const browserSessionOnly = parsed.browserSessionOnly === 'true';

    const expiringToken = uuidv1();

    let redirectToFinal;
    if (redirectTo.indexOf('/') !== 0) {
      if (!redirectTo.match(/^exp:\/\/[0-9a-zA-Z\-]+\.pennia\.[0-9a-zA-Z]+\.exp\.direct(:[0-9]+)?$/) && !redirectTo.match(/^pennidinh:\/\//)) {
        console.log('Unallowed redirectTo URL: ' + redirectTo);
        requestHelpers.failure(res, metric, 'Unallowed redirectTo URL: ' + redirectTo);
        return;
      }
      redirectToFinal = redirectTo + (redirectTo.indexOf('?') === -1 ? '?' : '&') + 'appSecretKey=' + appSecretKey; 
    } else {
      redirectToFinal = redirectTo;
    }
    console.log(redirectToFinal);

    fs.readFile('/keys/private.pem', 'utf-8', function(err, hubPrivateKey) {
        if (err) {
            requestHelpers.fault(res, metric, err);
            return;
        }

        encrypt.decryptWithPrivateKey(hubPrivateKey, pennidinhKey, res, metric, function (decryptedPennidinhKey) {
            const generationTime = decryptedPennidinhKey['generationTime'];

            if ((new Date().getTime() / 1000) - 60 > generationTime) {
                console.log('Passed in PenniDinh Key is too old. Must be less than 1 minute stale!');
                requestHelpers.failure(res, metric, 'Passed in PenniDinh Key is too old. Must be less than 1 minute stale!');
                return;
            }

            const handle = function (err) {
                if (err) {
                    requestHelpers.fault(res, metric, err);
                    return;
                }

                client.setex(expiringToken, 60 * 60, appSecretKey, function(err) {
                    if (err) {
                        requestHelpers.fault(res, metric, err);
                        return;
                    }

                    const t = new Date();
                    t.setSeconds(t.getSeconds() +  60 * 55);

                    res.statusCode = 303;
                    res.setHeader('Location', redirectToFinal);
                    res.setHeader('Set-Cookie', ['expiringToken=' + expiringToken + '; Expires=' + t + '; Path=/; Secure;']);
                    res.end();
                    metric.end();
                });
            };

            if (browserSessionOnly) {
                console.log('browser session login');
                client.setex(appSecretKey, 60 * 60, pennidinhKey.toString('base64'), handle);
            } else {
                console.log('non-browser session login');
                client.set(appSecretKey, pennidinhKey.toString('base64'), handle);
            }
        });
    });
}

module.exports.handleProfile = function(req, res, metric) {
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const expiringToken = parsed['pennidinh_token'].split(',')[0]; // bandaid fix where extra text is added to the pennidinh_token

    if (!expiringToken) {
        requestHelpers.failure(res, metric, 'pennidinh_token must be present!');
        return;
    }
    console.log('getting app secret key for expiring token: ' + expiringToken);

    client.get(expiringToken, function(err, appSecretKey) {
        if (err) {
            requestHelpers.fault(res, metric, err);
            return;
        }
        if (!appSecretKey) {
            requestHelpers.failure(res, metric, 'Couldnt find app secret key for expiring token: ' + expiringToken);
            return;
        }

        console.log('getting encryptedPenniDinh token for appSecretKey: ' + appSecretKey);

        getFreshEncryptedPenniDinhToken(appSecretKey, res, metric, function(encryptedPenniDinhToken) {
            console.log('getting hub private key');

            fs.readFile('/keys/private.pem', 'utf-8', function(err, hubPrivateKey) {
                if (err) {
                    requestHelpers.fault(res, metric, err);
                    return;
                }

                console.log('decrypting PenniDinh token');
                encrypt.decryptWithPrivateKey(hubPrivateKey, encryptedPenniDinhToken, res, metric, function (decryptedResponse) {
                    console.log('Decrypted decryptedResponse: ' + safeJsonStringify(decryptedResponse));

                    requestHelpers.succeed(res, metric, decryptedResponse['profile']);
                });
            });
        });
    });
}

module.exports.handleAuthenticationPing = function (req, res, metric) {
    res.setHeader('Content-Type', 'application/json');
    requestHelpers.succeed(res, metric, safeJsonStringify({}));
}

module.exports.handleAuthorizationPing = function (req, res, metric) {
    res.setHeader('Content-Type', 'application/json');
    requestHelpers.succeed(res, metric, safeJsonStringify({}));
}
