const DEBUG = false;
const safeJsonStringify = require('safe-json-stringify');

module.exports.failure = function(res, metric, message) {
    metric.end();
    res.statusCode = 400;

    if (message && !(Buffer.isBuffer(message) || typeof message === 'string')) {
      message = safeJsonStringify(message);
    }
     
    if (message) {
      console.log(message);
    }
    res.end(message);
}

module.exports.fault = function(res, metric, message) {
    metric.end();
    res.statusCode = 500;

    if (message && !(Buffer.isBuffer(message) || typeof message === 'string')) {
      message = safeJsonStringify(message);
    }

    if (message) {
      console.log(message);
    }

    res.end(message);
}

module.exports.notFound = function(res, metric, message) {
    metric.end();
    res.statusCode = 404;

    if (message && !(Buffer.isBuffer(message) || typeof message === 'string')) {
      message = safeJsonStringify(message);
    }

    if (DEBUG && message) {
      console.log(message);
    }
    res.end(message);
}

module.exports.succeed = function(res, metric, message) {
    metric.end();
    res.statusCode = 200;

    if (message && !(Buffer.isBuffer(message) || typeof message === 'string')) {
      message = safeJsonStringify(message);
    }

    if (DEBUG && message) {
      console.log(message);
    }
    res.end(message);
}
