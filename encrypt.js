const NodeRSA = require('node-rsa');
const requestHelpers = require("./request-helpers")

const fault = function(res, metric, message) {
    requestHelpers.failure(res, metric, message);
}

module.exports.encryptWithPublicKey = function( publicKeyStr, unencryptedStr, res, metric, successCallback) {
    const publicKey = new NodeRSA(publicKeyStr);

    try {
        successCallback(publicKey.encrypt(Buffer.from(unencryptedStr), 'base64'));
    } catch (e) {
        console.log(e);
        fault(res, metric, "Failed to encrypt message with public key...");
    }
}

module.exports.decryptWithPublicKey = function(publicKeyStr, encryptedStr, res, metric, successCallback) {
    const publicKey = new NodeRSA(publicKeyStr);

    let decryptedBodyStr;
    try {
        decryptedBodyStr = publicKey.decryptPublic(encryptedStr, 'utf8');
    } catch (e) {
        console.log(e);
        fault(res, metric, "Failed to decrypt message with public key...");
        return;
    }

    try {
        JSON.parse(decryptedBodyStr);
    } catch (e) {
        console.log(e);
        fault(res, metric, "Failed to parse JSON from decrypted text: " + decryptedBodyStr);
        return;
    }

    successCallback(JSON.parse(decryptedBodyStr));
}

module.exports.encryptWithPrivateKey = function(privateKeyStr, unencryptedStr, res, metric, successCallback) {
    const privateKey = new NodeRSA(privateKeyStr);

    try {
        successCallback(privateKey.encryptPrivate(Buffer.from(unencryptedStr), 'base64'));
    } catch (e) {
        console.log(e);
        fault(res, metric, "Failed to encrypt message with private key...");
    }
}

module.exports.decryptWithPrivateKey = function(privateKeyStr, encryptedStr, res, metric, successCallback) {
    const privateKey = new NodeRSA(privateKeyStr);

    let decryptedBodyStr;
    try {
        decryptedBodyStr = privateKey.decrypt(encryptedStr, 'utf8');
    } catch (e) {
        console.log(e);
        fault(res, metric, "Failed to decrypt message with private key...");
        return;
    }

    try {
        JSON.parse(decryptedBodyStr);
    } catch (e) {
        console.log(e);
        fault(res, metric, "Failed to parse JSON from decrypted text: " + decryptedBodyStr);
        return;
    }

    successCallback(JSON.parse(decryptedBodyStr));
}
