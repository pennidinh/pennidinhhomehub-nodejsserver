const redis = require("redis");
const requestHelpers = require("./request-helpers")
const safeJsonStringify = require('safe-json-stringify');
const queryString = require('query-string');
const uuidv1 = require('uuid/v1');

const client = redis.createClient(process.env.REDIS_ENDPOINT)
client.on("error", function (err) {
    console.log('Redis error: ' + err);
});

module.exports.getAdminUsers = function (req, res, metric) {
    client.smembers("admin-users", function(err, values) {
        if (err) {
            requestHelpers.fault(res, metric, err);
            return;
        }

        requestHelpers.succeed(res, metric, safeJsonStringify(values));
    });
}

const getCurrentUserEmail = function(req) {
    return req.headers['oidc_claim_email'];
};

function arraysEqual(a, b) {
    if (a === b) return true;
    if (a === null || b === null) return false;
    if (a.length !== b.length) return false;

    a.sort();
    b.sort();
    for (let i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) return false;
    }
    return true;
}

module.exports.removeAdminUsers = function (req, res, metric) {
    res.setHeader('Content-Type', 'application/json');
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const usersAsString = parsed.users;
    const users = JSON.parse(usersAsString);

    client.smembers("admin-users", function(err, savedUsers) {
        if (err) {
            requestHelpers.fault(res, metric, err);
            return;
        }

        if (users.includes(getCurrentUserEmail(req)) && !arraysEqual(savedUsers, users)) {
            requestHelpers.failure(res, metric, "Removing self from admin users is only allowed if the action would remove ALL admin users");
            return;
        }

        client.srem("admin-users", users, function() {
            client.save(function() {
                client.set("restart-apache", uuidv1(), function() {
                    requestHelpers.succeed(res, metric, safeJsonStringify({"message": "Successfully removed all users"}));
                });
            });
        });
    });
}

const VALID_EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const validateEmail = function(email) {
    return VALID_EMAIL_REGEX.test(String(email));
}

module.exports.addAdminUsers = function (req, res, metric) {
    res.setHeader('Content-Type', 'application/json');
    const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));
    const usersAsString = parsed.users;
    const users = JSON.parse(usersAsString);

    client.smembers("admin-users", function(err, savedUsers) {
        if (err) {
            requestHelpers.fault(res, metric, err);
            return;
        }

        if (!savedUsers.includes(getCurrentUserEmail(req)) && !users.includes(getCurrentUserEmail(req))) {
            requestHelpers.failure(res, metric, "You must add yourself as an admin user first!");
            return;
        }

        for (let i = 0; i < users.length; i++) {
            const user = users[i];
            const emailIsValid = validateEmail(user);

            if (!emailIsValid) {
                requestHelpers.fault(res, metric, safeJsonStringify({'message': 'Email is not a valid email: ' + user}))
                return;
            }
        }

        client.sadd("admin-users", users, function() {
            client.save(function() {
                client.set("restart-apache", uuidv1(), function() {
                    requestHelpers.succeed(res, metric, safeJsonStringify({"message": "Successfully added all users"}));
                });
            });
        });
    });
}
