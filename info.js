const requestHelpers = require("./request-helpers");
const safeJsonStringify = require('safe-json-stringify');
const dns = require('dns');
const redis = process.env.REDIS_ENDPOINT ? require("redis").createClient(process.env.REDIS_ENDPOINT) : require("redis").createClient();
redis.on("error", function (err) {
    console.log("Redis Client Error " + err);
});

const lastDNSFetched = {time: 0, value: ''};

module.exports.hostInfo = function (req, res, metric) {
    redis.get('external-subdomain', function(err, externalSubdomain) {
      if (err) {
        requestHelpers.fault(res, metric, 'fault while fetching external subdomain from redis');
        return;
      }

      const handle = function() {
        requestHelpers.succeed(res, metric, safeJsonStringify({domainName: externalSubdomain, internalIPAddress: process.env.INTERNAL_IP_ADDRESS, externalIPAddress: lastDNSFetched['value'].join(', ')}));
      }.bind(this);

      if (new Date().getTime() - lastDNSFetched['time'] > 1000 * 60) {
        dns.resolve4(externalSubdomain, function(err, addresses) {
          if (err) {
            console.log('err while fetching dns: ' + err.toString());
            handle();
          }

          lastDNSFetched['time'] = new Date().getTime();
          lastDNSFetched['value'] = addresses;
          handle();
        });
      } else {
        handle();
      }
    }.bind(this));

};
