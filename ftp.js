const redis = process.env.REDIS_ENDPOINT ? require("redis").createClient(process.env.REDIS_ENDPOINT) : require("redis").createClient();
redis.on("error", function (err) {
    console.log('Redis error: ' + err); });
const requestHelpers = require("./request-helpers")
const queryString = require('query-string');
const safeJsonStringify = require('safe-json-stringify');

module.exports.handleGetFtpUsers = function(req, res, metric) {
  redis.get('ftp-users', function(err, data) {
    if (err) {
      requestHelpers.fault(res, metric, 'error retrieving user info from redis');
      return;
    }

    if (!data) {
      requestHelpers.succeed(res, metric, [{username: 'pennidinh', password: null}]);
      return;
    }

    try {
      JSON.parse(data);
    } catch (e) {
      redis.set('ftp-users', safeJsonStringify([{username: 'pennidinh', password: null}]), function(err) {
        if (err) {
          requestHelpers.fault(res, metric, 'error deserializing users data. failed to reset it.');
          return;
        }

        requestHelpers.fault(res, metric, 'error deserializing users data. reset it. try again');
      });
      return;
    }

    const users = JSON.parse(data);
    requestHelpers.succeed(res, metric, users);
  });
};

module.exports.handleSetFtpUser = function(req, res, metric) {
  const parsed = queryString.parse(req.url.substring(req.url.indexOf('?')));

  const username = parsed['username'];
  const password = parsed['password'];

  if (username !== 'pennidinh') {
    requestHelpers.failure(res, metric, 'username param must be "pennidinh"');
    return;
  }

  if (!username.match(/^[a-z]+$/)) {
    requestHelpers.failure(res, metric, 'username contained space(s)');
    return;
  }
  if (password && password.match(/\s/)) {
    requestHelpers.failure(res, metric, 'password contained space(s)');
    return;
  }
  if (password && password.length >= 255) {
    requestHelpers.failure(res, metric, 'password greater than or equal to 255 characters');
    return;
  }

  // TODO fetch and de-dupe add
  redis.set('ftp-users', safeJsonStringify([{password: (password || ''), username: username}]), function(err) {
    if (err) {
      requestHelpers.fault(res, metric, 'error setting user info to redis');
      return;
    }

    module.exports.handleGetFtpUsers(req, res, metric);
  });
};
